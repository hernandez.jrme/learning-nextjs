import Head from 'next/head'

const About = () => {
    return ( 
        <>
    <Head>
    <title>Ninja List | About</title>
    <meta name="keywords" content="ninjas"/>
    </Head> 
        <div>
            <h1>About </h1>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Similique totam, aperiam odio voluptate repellat, iste suscipit numquam corporis neque enim nemo dolorem tempora. Non odio cumque iusto nam officia porro?</p>
        </div>
    </>
    );
}
 
export default About;